﻿using System;
using System.IO;
using System.Reflection;
using System.Text;
using Decal.Adapter;

namespace UBSandbox {
    [FriendlyName("UBSandbox")]
    public class PluginCore : PluginBase {
        public object PluginInstance;
        public Assembly CurrentAssembly;
        public Type PluginType;
        public FileSystemWatcher PluginWatcher = null;

        private bool needsReload = false;
        private DateTime lastFileChange = DateTime.UtcNow;

        public string PluginAssemblyNamespace = "UBSandbox.Plugin";
        public string PluginAssemblyName = "UBSandbox_Plugin.dll";
        public string PluginAssemblyPath = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);

        public bool PluginUnloaded = true;

        /// <summary>
        /// This is called when the plugin is started up.This happens only once.
        /// </summary>
        protected override void Startup() {
            PluginWatcher = new FileSystemWatcher {
                Path = PluginAssemblyPath,
                NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite,
                Filter = PluginAssemblyName
            };
            PluginWatcher.Changed += PluginWatcher_Changed;
            PluginWatcher.EnableRaisingEvents = true;

            Core.CharacterFilter.LoginComplete += CharacterFilter_LoginComplete;
            Core.CharacterFilter.Logoff += CharacterFilter_Logoff;

        }
        protected override void Shutdown() {
            UnloadPluginAssembly();
            if (PluginWatcher != null) PluginWatcher.Dispose();
            Core.CharacterFilter.LoginComplete -= CharacterFilter_LoginComplete;
            Core.CharacterFilter.Logoff -= CharacterFilter_Logoff;
            Core.RenderFrame -= Core_RenderFrame;
        }
        private void CharacterFilter_LoginComplete(object sender, EventArgs e) {
            LoadPluginAssembly();
        }

        private void CharacterFilter_Logoff(object sender, Decal.Adapter.Wrappers.LogoffEventArgs e) {
            if (e.Type == Decal.Adapter.Wrappers.LogoffEventType.Authorized) {
                UnloadPluginAssembly();
            }
        }

        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
                if (needsReload && DateTime.UtcNow - lastFileChange > TimeSpan.FromSeconds(3)) {
                    needsReload = false;
                    Core.RenderFrame -= Core_RenderFrame;
                    Core.Actions.AddChatText("Reloading " + PluginAssemblyName, 1);
                    UnloadPluginAssembly();
                    LoadPluginAssembly();
                }
            } catch (Exception ex) { LogException(ex); }
        }

        private void PluginWatcher_Changed(object sender, FileSystemEventArgs e) {
            if (needsReload == false) {
                Core.RenderFrame += Core_RenderFrame;
                needsReload = true;
            }
            lastFileChange = DateTime.UtcNow;
        }

        private void LoadPluginAssembly() {
            if (!PluginUnloaded) return;
            PluginUnloaded = false;
            try {
                var assemblyPath = System.IO.Path.Combine(PluginAssemblyPath, PluginAssemblyName);
                byte[] bits = File.ReadAllBytes(assemblyPath);
                CurrentAssembly = Assembly.Load(bits);
                PluginType = CurrentAssembly.GetType(PluginAssemblyNamespace);
                PluginInstance = Activator.CreateInstance(PluginType);
                PluginType.GetMethod("Startup")?.Invoke(PluginInstance, new object[] { assemblyPath, Host, Core });
            } catch (Exception ex) { LogException(ex); }
        }

        private void UnloadPluginAssembly() {
            if (PluginUnloaded) return;
            PluginUnloaded = true;
            try {
                PluginType.GetMethod("Shutdown")?.Invoke(PluginInstance, null);
                PluginInstance = null;
                CurrentAssembly = null;
                PluginType = null;
            } catch (Exception ex) { LogException(ex); }
        }

        public void LogException(Exception ex) {
            StringBuilder dummy = new StringBuilder("============================================================================\n");
            dummy.Append($"Error: {ex.Message}\n");
            dummy.Append($"Source: {ex.Source}\n");
            dummy.Append($"Stack: {ex.StackTrace}\n");
            if (ex.InnerException != null) {
                dummy.Append($"Inner: {ex.InnerException.Message}\n");
                dummy.Append($"Inner Stack: {ex.InnerException.StackTrace}\n");
            }
            if (!PluginUnloaded)
                WriteToChat(dummy.ToString(), 22);

            File.AppendAllText(System.IO.Path.Combine(PluginAssemblyPath, "exceptions.txt"), dummy.ToString());

            using (StreamWriter writer = new StreamWriter(System.IO.Path.Combine(PluginAssemblyPath, "exceptions.txt"), true)) {
                writer.Write(dummy.ToString());
                writer.Close();
            }
        }
        public void WriteToChat(string message, int color = 7) {
            if (!PluginUnloaded)
                Core.Actions.AddChatText("<Hax>: " + message, color);
            File.AppendAllText(System.IO.Path.Combine(PluginAssemblyPath, "log.txt"), $"{DateTime.Now.ToString("yy/MM/dd H:mm:ss")} <Hax>: {message}");
        }
    }
}
