﻿using System;
using Decal.Adapter;
using Decal.Adapter.Wrappers;
using MyClasses.MetaViewWrappers;
using AcClient;
using System.Runtime.InteropServices;

namespace UBSandbox {
    [WireUpBaseEvents]

    //[MVView("UBSandbox_Plugin.mainView.xml")]
    //[MVWireUpControlEvents]
    [FriendlyName("UBSandbox")]

    unsafe public class Plugin {
        public void Startup(string assemblyPath, PluginHost Host, CoreManager core) {
            try {
                Core.WriteToChat("Initializing...");
                Core.Startup(assemblyPath, System.IO.Path.GetDirectoryName(assemblyPath));

                Decal.Adapter.CoreManager.Current.CommandLineText += Core_CommandLineText;
                Decal.Adapter.CoreManager.Current.RenderFrame += Core_RenderFrame;
                Decal.Adapter.CoreManager.Current.CharacterFilter.SpellCast += CharacterFilter_SpellCast;


                Int64 retval = 0;
                CPhysicsObj.player_object->weenie_obj->m_pQualities->a0.a1.InqInt64(2, &retval);
                lastXP = retval;
                CPhysicsObj.player_object->weenie_obj->m_pQualities->a0.a1.InqInt64(6, &retval);
                lastLUM = retval;


                populate_tabbedSpells();

                ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64.Setup(new def_ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64(HOOK_ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64));
                //CPhysicsObj__stick_to_object.Setup(new def_CPhysicsObj__stick_to_object(HOOK_CPhysicsObj__stick_to_object));
                //CPhysicsObj__TurnToObject.Setup(new def_CPhysicsObj__TurnToObject(HOOK_CPhysicsObj__TurnToObject));
                //SmartBox__HandleDeleteObject.Setup(new def_SmartBox__HandleDeleteObject(HOOK_SmartBox__HandleDeleteObject));
                start_time = *Timer.cur_time;



            } catch (Exception ex) { Core.LogException(ex); }
        }



        #region xp hook
        // .text:006AF9DB                 call    ?Handle_Qualities__PrivateUpdateInt64@ClientObjMaintSystem@@QAEKEK_J@Z ; ClientObjMaintSystem::Handle_Qualities__PrivateUpdateInt64(uchar,ulong,__int64)
        //        public UInt32 Handle_Qualities__PrivateUpdateInt64(char wts, UInt32 stype, Int64 val) => ((delegate* unmanaged[Thiscall]<ref ClientObjMaintSystem, char, UInt32, Int64, UInt32>)0x00559C40)(ref this, wts, stype, val); // .text:00559000 ; unsigned int __thiscall ClientObjMaintSystem::Handle_Qualities__PrivateUpdateInt64(ClientObjMaintSystem *this, char wts, unsigned int stype, __int64 val) .text:00559000 ?Handle_Qualities__PrivateUpdateInt64@ClientObjMaintSystem@@QAEKEK_J@Z
        internal static Hook ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64 = new AcClient.Hook(0x00559C40, 0x006AF9DB);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate UInt32 def_ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64(ClientObjMaintSystem* This, char wts, UInt32 stype, Int64 val);
        public Int64 lastXP = 0;
        public Int64 accum_XP = 0;
        public Int64 lastLUM = 0;
        public Int64 accum_LUM = 0;
        public Double accum_time = 0;
        public Double start_time = 0;
        public UInt32 HOOK_ClientObjMaintSystem__Handle_Qualities__PrivateUpdateInt64(ClientObjMaintSystem* This, char wts, UInt32 stype, Int64 val) {

            Int64 delta = 0;
            if (((accum_XP + accum_LUM) > 0) && *Timer.cur_time > (accum_time + 1)) {
                Core.WriteToChat($"HOOK: Unclaimed {accum_XP} XP and {accum_LUM} LUM");
                accum_XP = 0;
                accum_LUM = 0;
            }
            accum_time = *Timer.cur_time;
            switch (stype) {
                case 2:
                    delta = val - lastXP;
                    //Core.WriteToChat($"HOOK: AVAILABLE_EXPERIENCE += {delta}");
                    if (delta > 0) {
                        accum_XP += delta;
                    }
                    lastXP = val;
                    break;
                case 6:
                    delta = val - lastLUM;
                    //Core.WriteToChat($"HOOK: AVAILABLE_LUMINANCE += {delta}");
                    if (delta > 0) {
                        accum_LUM += delta;
                    }
                    lastLUM = val;
                    break;
            }
            return This->Handle_Qualities__PrivateUpdateInt64(wts, stype, val);
        }
        #endregion

        //#region SmartBox::HandleDeleteObject
        //// .text:00451EE0 ; public: enum NetBlobProcessedStatus __thiscall SmartBox::HandleDeleteObject(class NetBlob *,unsigned long,unsigned short)
        //// .text:006AD631                 call    ?HandleDeleteObject@SmartBox@@QAE?AW4NetBlobProcessedStatus@@PAVNetBlob@@KG@Z ; SmartBox::HandleDeleteObject(NetBlob *,ulong,ushort)
        //internal static Hook SmartBox__HandleDeleteObject = new AcClient.Hook(0x00451EE0, 0x006AD631);
        //[UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate NetBlobProcessedStatus def_SmartBox__HandleDeleteObject(SmartBox* This, NetBlob* blob, UInt32 object_id, UInt16 instance_timestamp);
        //public NetBlobProcessedStatus HOOK_SmartBox__HandleDeleteObject(SmartBox* This, NetBlob* blob, UInt32 object_id, UInt16 instance_timestamp) {
        //    var retval = This->HandleDeleteObject(blob, object_id, instance_timestamp);
        //    //Core.WriteToChat($"HOOK:SmartBox::HandleDeleteObject(0x{(int)This:X8},0x{(int)blob:X8},0x{object_id:X8},{instance_timestamp})");
        //    if (((accum_LUM > 0) || (accum_XP > 0)) && touchedTargets.Contains(object_id)) {
        //        Core.WriteToChat($"HOOK: DeleteObject 0x{object_id:X8} for {accum_XP} XP and {accum_LUM} LUM");
        //        accum_XP = 0;
        //        accum_LUM = 0;
        //        touchedTargets.Remove(object_id);
        //    }
        //    return retval;
        //}



        //#endregion

        //#region CPhysicsObj::*ToObject

        //public System.Collections.Generic.HashSet<UInt32> touchedTargets = new System.Collections.Generic.HashSet<UInt32>();

        //// .text:005132E0 ; public: void __thiscall CPhysicsObj::stick_to_object(unsigned long)
        //// .text:00525189                 call    ?stick_to_object@CPhysicsObj@@QAEXK@Z ; CPhysicsObj::stick_to_object(ulong)
        //internal static Hook CPhysicsObj__stick_to_object = new AcClient.Hook(0x005132E0, 0x00525189);
        //[UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPhysicsObj__stick_to_object(CPhysicsObj* This, UInt32 _object_id);
        //public void HOOK_CPhysicsObj__stick_to_object(CPhysicsObj* This, UInt32 _object_id) {
        //    This->stick_to_object(_object_id);
        //    //Core.WriteToChat($"HOOK:CPhysicsObj::stick_to_object(0x{(int)This:X8},0x{_object_id:X8})");
        //    var actor = This->weenie_obj;
        //    if (actor->pwd._pet_owner != 0) {
        //        actor = CObjectMaint.s_pcInstance->GetWeenieObject(actor->pwd._pet_owner);
        //    }
        //    if (actor == null) return;
        //    if (actor->a0.a0.a0.id == *CPhysicsPart.player_iid || (ClientFellowshipSystem.s_pFellowshipSystem->m_pFellowship != null && ClientFellowshipSystem.s_pFellowshipSystem->IsFellow(actor->a0.a0.a0.id))) {
        //        touchedTargets.Add(_object_id);
        //    }
        //}

        //// .text:00513440 ; public: void __thiscall CPhysicsObj::TurnToObject(unsigned long,class MovementParameters const &)
        //// .text:005252D0                 call    ?TurnToObject@CPhysicsObj@@QAEXKABVMovementParameters@@@Z ; CPhysicsObj::TurnToObject(ulong,MovementParameters const &)
        //internal static Hook CPhysicsObj__TurnToObject = new AcClient.Hook(0x00513440, 0x005252D0);
        //[UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPhysicsObj__TurnToObject(CPhysicsObj* This, UInt32 _object_id, MovementParameters* _params);
        //public void HOOK_CPhysicsObj__TurnToObject(CPhysicsObj* This, UInt32 _object_id, MovementParameters* _params) {
        //    This->TurnToObject(_object_id, _params);
        //    Core.WriteToChat($"HOOK:CPhysicsObj::TurnToObject(0x{(int)This:X8},0x{_object_id:X8},0x{(int)_params:X8})");
        //    var actor = This->weenie_obj;
        //    if (actor->pwd._pet_owner != 0) {
        //        actor = CObjectMaint.s_pcInstance->GetWeenieObject(actor->pwd._pet_owner);
        //    }
        //    if (actor == null) return;
        //    if (actor->a0.a0.a0.id == *CPhysicsPart.player_iid || (ClientFellowshipSystem.s_pFellowshipSystem->m_pFellowship != null && ClientFellowshipSystem.s_pFellowshipSystem->IsFellow(actor->a0.a0.a0.id))) {
        //        touchedTargets.Add(_object_id);
        //    }
        //}

        //#endregion




        /*
                public override string ToString() {
            if (head == null) return "null";
            System.Text.StringBuilder Chars = new System.Text.StringBuilder();
            PackableLLNode<T>* iter = head;
            while (iter != null) {
                Chars.Append("(" + iter->data.ToString() + "), ");
                iter = iter->next;
            }
            return $"curNum:{curNum}, {Chars.ToString(0, Chars.Length - 2)}";
        }
        */

        /// <summary>
        /// run this any time spells update... or not :shrug:
        /// </summary>
        /// <param name="_in"></param>
        public System.Collections.Generic.HashSet<int> iterate_favorite_spells(PackableList<uint>* _in) {
            System.Collections.Generic.HashSet<int> ret = new System.Collections.Generic.HashSet<int>();
            PackableLLNode<uint>* iter = _in->head;
            while (iter != null) {
                ret.Add((int)iter->data);
                iter = iter->next;
            }
            return ret;
        }

        public void populate_tabbedSpells() {
            var pm = CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule;
            tabbedSpells[0] = iterate_favorite_spells(&pm.favorite_spells_0);
            tabbedSpells[1] = iterate_favorite_spells(&pm.favorite_spells_1);
            tabbedSpells[2] = iterate_favorite_spells(&pm.favorite_spells_2);
            tabbedSpells[3] = iterate_favorite_spells(&pm.favorite_spells_3);
            tabbedSpells[4] = iterate_favorite_spells(&pm.favorite_spells_4);
            tabbedSpells[5] = iterate_favorite_spells(&pm.favorite_spells_5);
            tabbedSpells[6] = iterate_favorite_spells(&pm.favorite_spells_6);
            tabbedSpells[7] = iterate_favorite_spells(&pm.favorite_spells_7);
        }
        public System.Collections.Generic.HashSet<int>[] tabbedSpells = new System.Collections.Generic.HashSet<int>[8];
        public string[] numerals = new string[] { "I", "II", "III", "IV", "V", "VI", "VII", "VIII" };
        private void CharacterFilter_SpellCast(object sender, SpellCastEventArgs e) {
            var cSpellBase = ClientMagicSystem.s_pMagicSystem->spellTable->GetSpellBase((uint)e.SpellId);
            var myUI = (gmSpellcastingUI*)GlobalEventHandler.geh->ResolveHandler(5100110);
            SpellCastSubMenu* menu;
            switch (cSpellBase->_school) {
                case 5: // Void Magic
                    menu = &myUI->m_subMenus_4;
                    break;
                case 1: // War Magic
                    menu = &myUI->m_subMenus_4;
                    break;
                case 2: // Life Magic
                    menu = &myUI->m_subMenus_5;
                    break;
                case 4: // Creature Enchantment
                    menu = &myUI->m_subMenus_6;
                    break;
                case 3: // Item Enchantment
                    menu = &myUI->m_subMenus_7;
                    break;
                default: // None
                    menu = &myUI->m_subMenus_0;
                    break;
            }
            if (!tabbedSpells[menu->m_tabIndex].Contains(e.SpellId)) {
                tabbedSpells[menu->m_tabIndex].Add(e.SpellId);
                var newpos = (int)menu->m_numSpells;

                AC1Legacy.PStringBase<char> name = new AC1Legacy.PStringBase<char>();
                ClientMagicSystem.s_pMagicSystem->GetSpellName(&name, (uint)e.SpellId);

                Core.WriteToChat($"Adding {name} (0x{e.SpellId:X4}) to tab {numerals[menu->m_tabIndex]}, index {newpos}");

                // add favorite in client
                menu->AddFavorite((uint)e.SpellId, newpos, 0);

                // update server
                CM_Character.Event_AddSpellFavorite((uint)e.SpellId, newpos, menu->m_tabIndex);

                // update selected spell name
                myUI->UpdateCastButtonTooltip();
                // update selected icon
                myUI->UpdateEndowmentIcon();
            }
        }

        public void Shutdown() {
            try {
                MVWireupHelper.WireupEnd(this);
                AcClient.Hook.Cleanup();
                //Decal.Adapter.CoreManager.Current.WindowMessage -= Current_WindowMessage;
                Decal.Adapter.CoreManager.Current.CommandLineText -= Core_CommandLineText;
                Decal.Adapter.CoreManager.Current.RenderFrame -= Core_RenderFrame;
                Decal.Adapter.CoreManager.Current.CharacterFilter.SpellCast -= CharacterFilter_SpellCast;
            } catch (Exception ex) { Core.LogException(ex); }
        }

        unsafe void Core_CommandLineText(object sender, ChatParserInterceptEventArgs e) {
            try {
                if (e.Text.Equals("/tloc")) {
                    e.Eat = true;
                    ////CObjectMaint * cObjMaint = (CObjectMaint*)*(Int32*)0x00842ADC;

                    ////HashBase_unsigned_long_ weenie_object_table = cObjMaint->weenie_object_table.hashBase_unsigned_long_;


                    ////Core.WriteToChat($"weenie_object_table vfptr:{(Int32)weenie_object_table.vfptr:X8} table_size:{weenie_object_table.table_size} table_mask:{weenie_object_table.table_mask:X8} key_shift:{weenie_object_table.key_shift:X8} fPlacementNew_:{weenie_object_table.fPlacementNew_:X8} buckets:{(Int32)weenie_object_table.buckets:X8}");
                    ////Core.WriteToChat($"weenie_object_table: {(Int32)cObjMaint->weenie_object_table.hashBase_unsigned_long_.table_size:X8}");


                    ////CM_Fellowship.Event_Dismiss(Sparky);

                    ////CM_Fellowship.Event_Recruit(Sparky);

                    ////ClientTradeSystem.GetTradeSystem()->AttemptToOpenTradeNegotiations(Sparky);
                    ////ClientTradeSystem.GetTradeSystem()->AttemptToTradeItem(Sparky, Yonneh);
                    ////Core.WriteToChat($"You are trading {ClientTradeSystem.GetTradeSystem()->GetNumSelfObjectsInTrade()} items");

                    ////AcClient.ClientCombatSystem* combatSystem = AcClient.ClientCombatSystem.GetCombatSystem();

                    ////combatSystem->ToggleCombatMode();
                    ////combatSystem->SetRequestedAttackHeight(ATTACK_HEIGHT.LOW_ATTACK_HEIGHT);

                    ////CM_Combat.SendNotice_AttackHeightChanged(ATTACK_HEIGHT.LOW_ATTACK_HEIGHT);

                    ////CM_Combat.Event_TargetedMeleeAttack(*P.ACCWeenieObject__selectedID, ATTACK_HEIGHT.LOW_ATTACK_HEIGHT, 1);

                    ////UInt32 Yonneh = 0x500007B3;
                    ////AcClient.CPlayerSystem.GetPlayerSystem()->LogOnCharacter(Yonneh);


                    //// CPhysicsObj* Physics = P.Call_CObjectMaint__GetObjectA(*P.ACCWeenieObject__selectedID);
                    //// Core.WriteToChat($"_containerID {Physics->weenie_obj->pwd._containerID:X8}");
                    //// Core.WriteToChat($"T LOC: [{Core.PStringBase_Char_read(Physics->weenie_obj->pwd._name)}] {Physics->id:X8} Physics: {(Int32)Physics:X8} Weenie: {(Int32)Physics->weenie_obj:X8}");
                    ////Core.WriteToChat($"x:{Physics->m_position_x:n5} y:{Physics->m_position_y:n5} z:{Physics->m_position_z:n5} Other Weenie: {(Int32)Physics->weenie_obj:X8}");
                    ////Core.WriteToChat($"Weenie's physics: {(Int32)Weenie->_phys_obj:X8} Weenie's Container: {Weenie->pwd._containerID:X8}");


                    ////UInt32 Sparky = 0x50000851;
                    //ClientFellowshipSystem* cfel = ClientFellowshipSystem.GetFellowshipSystem(); //(ClientFellowshipSystem*)*(Int32*)0x0087150C;
                    //if (&cfel->m_pFellowship->fellowship == null) {
                    //    Core.WriteToChat($"No fellowship found.");
                    //} else {
                    //    Core.WriteToChat($"fel.GetLeadersLevel(): {cfel->m_pFellowship->fellowship.GetLeadersLevel()}");
                    //    Core.WriteToChat($"fel._name: {cfel->m_pFellowship->fellowship._name}");
                    //}
                    ////UIFlow* uiFlow = UIFlow.m_instance;
                    //UIFlow* uiFlow = (UIFlow*)*(Int32*)0x0083E72C;
                    //Core.WriteToChat($"name: {uiFlow->_data->_CharSet}");


                    ////Core.WriteToChat($"CharacterSet: {uiFlow->_data->_CharSet}");
                    ////Core.WriteToChat($"UIPersistantData: {*uiFlow->_data}"); // [UB] UIPersistantData: _CharSet(set_(num:2, [0x500007B3] Yonneh, [0x5000084B] N O), delSet_(null), status:0, maxChars:11, account:(yonneh (DM:0 TOD:0 PreOrderTOD:0)), UseTurbineChat:1, HasTOD:1),_receivedSet:1,m_iidSelectedAvatar:500007B3

                    ////Core.WriteToChat($"Current UI: {uiFlow->_curMode}");

                    ////Core.WriteToChat($"len CLostCell {(sizeof (CLostCell))}");

                    //CObjectMaint* cObjMaint = (CObjectMaint*)*(Int32*)0x00842ADC;
                    //Core.WriteToChat($"got cObjMaint of {(Int32)&(cObjMaint->_interface.vfptr):X8}");

                    //CObjectMaint.GetObjectA(cObjMaint, 0x500007B3);
                    //Core.WriteToChat($"the price of a HashList_UIRegionPTR_UIRegionPTR_1_ is {sizeof(HashList_UIRegionPTR_UIRegionPTR_1_)}");
                    //Core.WriteToChat($"the price of a HashList<UIRegion, UIRegion> is {sizeof(HashList<UIRegion, UIRegion>)}");
                    //Core.WriteToChat($"the price of a UIRegion {sizeof(UIRegion)}");

                    ////ex get weenie by id
                    //if (*ACCWeenieObject.selectedID == 0) {
                    //    Core.WriteToChat("no item selected");
                    //} else {
                    //    ACCWeenieObject* weenie = (ACCWeenieObject*)cObjMaint->weenie_object_table.GetByID(*ACCWeenieObject.selectedID);
                    //    Core.WriteToChat($"0x{*ACCWeenieObject.selectedID:X8} is a fine selection.");
                    //    Core.WriteToChat($"I like to call it {weenie->pwd._name}");
                    //    Core.WriteToChat($"but you can call it {*weenie}");
                    //}
                    ////ex get a List<> of all client-known weenie ID's
                    //Core.WriteToChat($"object_table count {cObjMaint->object_table.GetList().Count}");
                    //Core.WriteToChat($"null_object_table count {cObjMaint->null_object_table.GetList().Count}");
                    //Core.WriteToChat($"weenie_object_table count {cObjMaint->weenie_object_table.GetList().Count}");
                    //Core.WriteToChat($"null_weenie_object_table count {cObjMaint->null_weenie_object_table.GetList().Count}");



                    ////List<Int32> poop = cObjMaint->object_inventory_table.GetList();
                    ////Core.WriteToChat($"object_inventory_table count {poop.Count}");
                    ////for (Int32 i = 0; i < poop.Count; i++) {
                    ////    CObjectInventory* f = (CObjectInventory*)poop[i];
                    ////    Core.WriteToChat($"Item {i}: {*f}");
                    ////}



                    ////Core.WriteToChat($"found {(Int32)&(GlobalEventHandler.GetGlobalEventHandler()->noticeRegistrar):X8} buckets");
                    ////Core.WriteToChat($"found {GlobalEventHandler.GetGlobalEventHandler()->noticeRegistrar.m_handlers->m_Int32rusiveTable.m_numBuckets} buckets");
                    ////Core.WriteToChat($"found {GlobalEventHandler.GetGlobalEventHandler()->noticeRegistrar.m_handlers->m_Int32rusiveTable.m_numElements} elements");
                    ////Core.WriteToChat($"found {GlobalEventHandler.GetGlobalEventHandler()->noticeRegistrar.m_handlers->m_Int32rusiveTable->} elements");
                    ////Core.WriteToChat($"found {(Int32)GlobalEventHandler.GetGlobalEventHandler()->ResolveHandler(0x4DD1F9):X8} handler 0");

                    ////Core.WriteToChat($"CPlayerSystem: {CPlayerSystem.GetPlayerSystem()->ToString()}"); // [UB] CPlayerSystem: account_:yonneh (DM:0 TOD:0 PreOrderTOD:0),playerID:500007B3,inventoryMask:7C8FFFFF,clothingPriorityMask:0003FF7E,lastFullyMergedSrcID:00000000,lastFullyMergedDstID:00000000,blockingID:00000000,blockedID:00000000,blockingDestID:00000000,blockedSpellTargetID:00000000,blockedSpellID:00000000,blockedSide:SLOT_SIDE_NULL,unblockAttemptNum:0,mOpenContainerID:500007B3,logOnRequestTime:0.00,logOffRequestTime:0.00,logOffTime:0.00,deleteCharRequestTime:0.00,playerInitTime:724,003,383.76,expirationTime:0.00,m_fLoad:0.00,m_accountHasThroneofDestiny initialLoginComplete allContainedObjectsReceived player_initialized player_desc_received 

                    //Core.WriteToChat($"*CPhysicsPart.player_iid: {*CPhysicsPart.player_iid:X8}");
                    ////Core.WriteToChat($"ClientObjMaintSystem.GetWeenieObject(0x500007B3): {ClientObjMaintSystem.GetWeenieObject(0x500007B3)->cWeenieObject.hash.id:X8}");
                    ////Core.WriteToChat($"CObjectMaint.GetObjectA(cObjMaint, 0x500007B3): {CObjectMaint.GetObjectA(cObjMaint, 0x500007B3)->hash.hash.id:X8}");
                    //Core.WriteToChat($"(*CPhysicsObj.player_object)->m_position: {(*CPhysicsObj.player_object)->m_position}");


                    //Core.WriteToChat($"Your SmartBox is: {SmartBox.smartbox->ToString()}");


                    //Core.WriteToChat($"Packet loss is at {Client.m_instance->LinkStatusHolder.m_fPacketLoss:n2}%");
                    //Core.WriteToChat($"*Client.m_instance {*Client.m_instance}");
                    //Core.WriteToChat($"Client.m_instance->LinkStatusHolder {Client.m_instance->LinkStatusHolder}");

                    Core.WriteToChat($"sizeof(LongHash<CPhysicsObj>): {sizeof(LongHash<CPhysicsObj>)}");
                    Core.WriteToChat($"sizeof(LongHash<CWeenieObject>): {sizeof(LongHash<CWeenieObject>)}");
                    Core.WriteToChat($"CObjectMaint.s_pcInstance->object_table.hash.Count: {CObjectMaint.s_pcInstance->object_table.a0.Count}");
                    Core.WriteToChat($"CObjectMaint.s_pcInstance->object_table.hash.Contains(*CPhysicsPart.player_iid): {CObjectMaint.s_pcInstance->object_table.a0.Contains(*CPhysicsPart.player_iid)}");
                    Core.WriteToChat($"CObjectMaint.s_pcInstance->object_table.hash.Contains(0): {CObjectMaint.s_pcInstance->object_table.a0.Contains(0)}");
                    UInt32[] array = new UInt32[CObjectMaint.s_pcInstance->object_table.a0.Count];
                    CObjectMaint.s_pcInstance->object_table.a0.CopyTo(array, 0);
                    Core.WriteToChat($"array: {array[1]:X8}");

                    Core.WriteToChat($"Client.m_instance->m_hostName (PStringBase<char>) {Client.m_instance->m_hostName}");
                    Core.WriteToChat($"Client.m_instance->m_worldName (PStringBase<ushort>) {Client.m_instance->m_worldName}");

                    //var poo = AC1Legacy.PStringBase<char>.newCharfromstring("Well that worked swimmingly!");
                    //Core.WriteToChat($"AC1Legacy.PstringBase<Char>: {poo}");
                    //var poo2 = AC1Legacy.PStringBase<UInt16>.newCharfromstring("Well that worked swimmingly!");
                    //Core.WriteToChat($"AC1Legacy.PstringBase<UInt16>: {poo2}");
                    AC1Legacy.PStringBase<char> foo = "hello world";
                    Core.WriteToChat($"AC1Legacy.PStringBase<char> foo = \"hello world\": {foo}=");
                    AC1Legacy.PStringBase<UInt16> foo2 = "hello world";
                    Core.WriteToChat($"AC1Legacy.PStringBase<UInt16> foo2 = \"hello world\": {foo2}=");
                    int fooo = 47;
                    AC1Legacy.PStringBase<char> fooo_c = fooo;
                    Core.WriteToChat($"Fourty-Seven = {fooo_c}=");
                    for (int i = 0; i < CObjectMaint.s_pcInstance->object_inventory_table.a0.Count; i++) {
                        Core.WriteToChat($"");
                        Core.WriteToChat($"==== inventory item {i}\n{((CObjectInventory*)CObjectMaint.s_pcInstance->object_inventory_table.a0[i])->ToString()}");
                    }
                    Core.WriteToChat($"selected item = {((ACCWeenieObject*)CObjectMaint.s_pcInstance->weenie_object_table.a0.GetByID(*ACCWeenieObject.selectedID))->ToString()}=");

                    Core.WriteToChat($"-- CLEAN EXIT");
                    return;
                }
                if (e.Text.Equals("/com")) {
                    e.Eat = true;

                    //Core.WriteToChat($"Player Object: {(int)CPhysicsObj.player_object:X8}");
                    //Core.WriteToChat($"position: {(int)CPhysicsObj.player_object:X8}");

                    //Core.WriteToChat($"GetObj: {(int)CPhysicsObj.GetObjectA(0x50004330):X8}");
                    //CPhysicsObj.GetObjectA(0x50004330)->play_script(PScriptType.PS_AttribUpRed, 1f);
                    //CPhysicsObj.GetObjectA(0x5000026D)->play_script(PScriptType.PS_SkillUpRed, 1f);
                    CPhysicsObj.player_object->play_script(PScriptType.PS_SkillUpRed, 1f);

                    return;
                }
                if (e.Text.Equals("/sel")) {
                    e.Eat = true;
                    var stuck_to = CObjectMaint.s_pcInstance->GetWeenieObject(*ACCWeenieObject.selectedID);
                    if (stuck_to != null) {
                        Core.WriteToChat($"{*stuck_to}");
                    } else {
                        Core.WriteToChat($"null");
                    }
                    return;
                }


                if (e.Text.Equals("/cp")) {
                    e.Eat = true;

                    //Core.WriteToChat($"Player Object: {(int)CPhysicsObj.player_object:X8}");
                    //Core.WriteToChat($"position: {(int)CPhysicsObj.player_object:X8}");
                    //var v0 = GlobalEventHandler.GetGlobalEventHandler();

                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_0: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_0}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_1: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_1}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_2: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_2}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_3: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_3}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_4: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_4}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_5: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_5}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_6: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_6}");
                    Core.WriteToChat($"CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_7: {CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.favorite_spells_7}");
                    //CPlayerSystem.s_pPlayerSystem->playerModule.PlayerModule.AddSpellFavorite(1635, 0, 7);
                    //CM_Character.Event_AddSpellFavorite(1635, 0, 7);

                    //gmSpellcastingUI* mine = (gmSpellcastingUI*)GlobalEventHandler.geh->ResolveHandler(5100110);
                    //Core.WriteToChat($"mine->m_subMenus_7: {mine->m_subMenus_7}");
                    //mine->m_subMenus_7.AddFavorite(1635, 1, 1);


                    return;
                }
                if (e.Text.Equals("/gm")) {
                    e.Eat = true;

                    //Core.WriteToChat($"Player Object: {(int)CPhysicsObj.player_object:X8}");
                    //Core.WriteToChat($"position: {(int)CPhysicsObj.player_object:X8}");
                    //var v0 = GlobalEventHandler.GetGlobalEventHandler();
                    gmSpellcastingUI* mine = (gmSpellcastingUI*)GlobalEventHandler.geh->ResolveHandler(5100110);
                    Core.WriteToChat($"0x{(int)mine:X8}: {mine->ToString()}");
                    Core.WriteToChat($"is: {sizeof(gmNoticeHandler)}");

                    return;
                }



                if (e.Text.StartsWith("/combatlvl ")) {
                    e.Eat = true;
                    string second = e.Text.Replace("/combatlvl ", "");
                    Single.TryParse(second, out Single level);
                    if (level < 0) level = 0;
                    if (level > 1) level = 1;
                    Core.WriteToChat($"Setting Attack Power to {level:n6}");
                    ClientCombatSystem.s_pCombatSystem->m_rUIRequestedPower = level;
                    CM_Combat.SendNotice_DesiredAttackPowerChanged(level);

                    return;
                }
                if (e.Text.StartsWith("/combatap ")) {
                    e.Eat = true;
                    string second = e.Text.Replace("/combatap ", "");
                    AcClient.ATTACK_HEIGHT height;
                    try {
                        height = (AcClient.ATTACK_HEIGHT)Enum.Parse(typeof(AcClient.ATTACK_HEIGHT), second, true);
                    } catch {
                        Core.Error($"Invalid option ({second}). Valid values are: {string.Join(", ", Enum.GetNames(typeof(AcClient.ATTACK_HEIGHT)))}");
                        return;
                    }
                    Core.WriteToChat($"Setting Attack Level to {height}");

                    ClientCombatSystem.s_pCombatSystem->SetRequestedAttackHeight(height);
                    CM_Combat.SendNotice_AttackHeightChanged(ClientCombatSystem.s_pCombatSystem->requestedAttackHeight);

                    //ClientCombatSystem.s_pCombatSystem->StartAttackRequest();
                    ClientCombatSystem.s_pCombatSystem->EndAttackRequest(ClientCombatSystem.s_pCombatSystem->requestedAttackHeight, -1.0f);

                    return;
                }





            } catch (Exception ex) { Core.LogException(ex); }
        }


        // Don't look at me like that. I see you looking.
        //public unsafe static string InqString(Int32 stype) {
        //    Int32 ptr;
        //    try { ptr = *(Int32*)(P.CBaseQualities + 0x0018); } catch { return null; }
        //    if (ptr == 0) return null;
        //    Int32 this_bucket = *(Int32*)((*(Int32*)(ptr + 0x08)) + ((stype % (*(Int32*)(ptr + 0x0C))) << 2));
        //    if (this_bucket == 0) { return null; }
        //    while (*(Int32*)this_bucket != stype) {
        //        this_bucket = *(Int32*)(this_bucket + 0x08);
        //        if (this_bucket == 0) { return null; }
        //    }
        //    if (this_bucket == 0 || *(Int32*)this_bucket != stype)
        //        return null;
        //    return Core.PStringBase_Char_read((Int32*)(this_bucket + 0x04));
        //}


        //internal static Int32 ResolveHandler(Int32 handlerID) {
        //    try {
        //        Int32 m_handlers = *(Int32*)((*(Int32*)(*P.GlobalEventHandler + 4)) + 8 + ((handlerID % 0x17) << 2));
        //        while (*(Int32*)m_handlers != handlerID) m_handlers = *(Int32*)(m_handlers + 4);
        //        return *(Int32*)*(Int32*)(*(Int32*)(m_handlers + 8) + 4);
        //    } catch { return 0; }
        //}


        //internal static Int32 GetSelfPtr {
        //get {
        //        try {
        //            Int32 ptr = GetMonarchPtr;
        //            while (ptr != 0 && *(Int32*)(ptr + 0x0014) != *P.CPhysicsPart__player_iid) {
        //                ptr = *(Int32*)(ptr + 0x000C);
        //            }
        //            return ptr;
        //        } catch { return 0; }
        //    }
        //}
        //internal static Int32 GetPatronPtr { get { try { return *(Int32*)(GetSelfPtr + 0x0004); } catch { return 0; } } }
        //internal static Int32 GetMonarchPtr { get { try { return *(Int32*)(*P.ClientAllegianceSystem + 0x001C); } catch { return 0; } } }
        //internal static Int32 MonarchFollowers { get { try { return *(Int32*)(*P.ClientAllegianceSystem + 0x0120) - 1; } catch { return 0; } } }
        //internal static Int32 YourFollowers { get { try { return *(Int32*)(*P.ClientAllegianceSystem + 0x0124); } catch { return 0; } } }

        //public static string Name { get { try { return Core.PStringBase_Char_read((Int32*)((*P.ClientAllegianceSystem) + 0x003C)); } catch { return ""; } } }


        private void Core_RenderFrame(object sender, EventArgs e) {
            try {
            } catch (Exception ex) { Core.LogException(ex); }
        }

        [MVControlEvent("DisplayInv", "Change")]
        void DisplayInv_Change(object sender, MVCheckBoxChangeEventArgs e) {
            Core.WriteToChat($"DisplayInv Change {e.Id} {e.Checked}");

        }


    }


}
