﻿using System;
using System.Diagnostics;
using System.IO;
using System.Runtime.InteropServices;

namespace UBSandbox {
    public static unsafe class Core {
        public static readonly int version = 1912050000;
        internal static string assemblyPath = null;
        private static string pluginDirectory = null;
        public static void Startup(string assemblyPath = null, string storagePath = null) {
            Core.assemblyPath = Path.GetDirectoryName(assemblyPath);
            pluginDirectory = storagePath;
            //Core.assemblyPath = assemblyPath;
            //Write(P.ResolutionPatchWidth, 0); // minimum width
            //Write(P.ResolutionPatchHeight, 0); // minimum height
            //ConfirmationRequest.Enable();
            //Vendor.Enable();
            //RadarHookSetup();
        }

        public static void Shutdown() {
            //ConfirmationRequest.Disable();
            //Vendor.Disable();
            //ActionQueue.Dispose();
            //Jumper.JumpCancel();
            //RadarHookRemove();
        }

        public delegate void callback();


        internal static void WriteToChat(string message, int color = 5) {
            try {
                string msg = "[UB] " + message;
                Decal.Adapter.CoreManager.Current?.Actions?.AddChatText(msg, color);
                //vTank.Tell(msg, color, 0);
                WriteToDebugLog(message);
            } catch (Exception ex) { LogException(ex); }
        }
        public static void WriteToDebugLog(string message) {
            File.AppendAllText(Path.Combine(pluginDirectory, $"debug.{DateTime.Now.ToString("yyyy-MM-dd")}.txt"), $"{DateTime.Now.ToString("yy/MM/dd H:mm:ss")} {message}" + Environment.NewLine);
        }
        internal static void Error(string message) {
            WriteToChat("Error: " + message, 15);
        }
        public static void LogException(Exception ex) {
            try {
                using (StreamWriter writer = new StreamWriter(Path.Combine(assemblyPath, "exceptions.txt"), true)) {

                    writer.WriteLine("============================================================================");
                    writer.WriteLine("UBHelper: " + ex.ToString());
                    writer.WriteLine("============================================================================");
                    writer.WriteLine(DateTime.Now.ToString());
                    writer.WriteLine("Error: " + ex.Message);
                    writer.WriteLine("Source: " + ex.Source);
                    writer.WriteLine("Stack: " + ex.StackTrace);
                    if (ex.InnerException != null) {
                        writer.WriteLine("Inner: " + ex.InnerException.Message);
                        writer.WriteLine("Inner Stack: " + ex.InnerException.StackTrace);
                    }
                    writer.WriteLine("============================================================================");
                    writer.WriteLine("");
                    writer.Close();
                }
            } catch { }
        }

        #region PStringBase_char_read(int* pointer)
        internal static string PStringBase_char_read(int pointer) {
            if (pointer == 0) {
                Error("I don't like zero, it scares me.");
                return "";
            }
            try {
                return Marshal.PtrToStringAnsi((IntPtr)(pointer + 0x14), (*(int*)(pointer + 0x08)) - 1);
            } catch { return ""; }
        }
        #endregion
        #region PSRefBufferCharData_char_read(int* pointer)
        internal static string PSRefBufferCharData_char_read(int* pointer) {
            try {
                return Marshal.PtrToStringAnsi((IntPtr)(*pointer));
            } catch { return ""; }
        }
        #endregion
        #region PSRefBufferCharData_ushort_read(int* pointer)
        internal static string PSRefBufferCharData_ushort_read(int* pointer) {
            try {
                return Marshal.PtrToStringUni((IntPtr)(*pointer));
            } catch { return ""; }
        }
        #endregion
        #region Misc Strings
        //internal static string CharacterName { get { try { return PStringBase_char_read((int*)(*(int*)(P.CPhysics + 0x012C) + 0x009C)); } catch { return ""; } } }
        //internal static string UserName { get { try { return PStringBase_char_read((int*)(*P.gmClient + 0x0074)); } catch { return ""; } } }
        //internal static string WorldName { get { try { return PSRefBufferCharData_ushort_read((int*)(*P.gmClient + 0x00A8)); } catch { return ""; } } }
        //internal static string ServerAddress { get { try { return PSRefBufferCharData_char_read((int*)(*P.gmClient + 0x008C)); } catch { return ""; } } }
        //internal static string Password { get { try { return PSRefBufferCharData_char_read((int*)(*P.gmClient + 0x0158)); } catch { return ""; } } }
        #endregion



    }
}
