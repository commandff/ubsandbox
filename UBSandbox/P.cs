﻿using System;
using System.Runtime.InteropServices;
using System.Diagnostics;
using AcClient;

namespace UBSandbox {
    internal unsafe static class P {
        internal static readonly UIntPtr doublesize = new UIntPtr(sizeof(double));
        internal static readonly UIntPtr longsize = new UIntPtr(sizeof(long));
        internal static readonly UIntPtr intsize = new UIntPtr(sizeof(int));
        internal static readonly UIntPtr floatsize = new UIntPtr(sizeof(float));
        internal static readonly UIntPtr shortsize = new UIntPtr(sizeof(short));
        internal static readonly UIntPtr bytesize = new UIntPtr(sizeof(byte));

        internal static readonly IntPtr ResolutionPatchWidth = (IntPtr)0x00439381;
        internal static readonly IntPtr ResolutionPatchHeight = (IntPtr)0x0043938B;




        internal static int CBaseQualities { get { try { return *(int*)((*(int*)((*CPhysicsPart__player_object) + 0x012C)) + 0x014C) + 0x0038; } catch { return 0; } } }
        internal static int Call_CBaseQualities__InqInt(int CBaseQualities, int stype, int* retval, int raw, int allow_negative) => ((def_CBaseQualities__InqInt)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590C20, typeof(def_CBaseQualities__InqInt)))(CBaseQualities, stype, retval, raw, allow_negative);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInt(int CBaseQualities, int stype, int* retval, int raw, int allow_negative); // int __thiscall CBaseQualities::InqInt(CBaseQualities *this, unsigned int stype, int *retval, int raw, int allow_negative)
        internal static int Call_CBaseQualities__InqInt64(int CBaseQualities, int stype, long* retval) => ((def_CBaseQualities__InqInt64)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590C70, typeof(def_CBaseQualities__InqInt64)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInt64(int CBaseQualities, int stype, long* retval); // int __thiscall CBaseQualities::InqInt64(CBaseQualities *this, unsigned int stype, __int64 *retval)
        internal static int Call_CBaseQualities__InqBool(int CBaseQualities, int stype, bool* retval) => ((def_CBaseQualities__InqBool)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590CA0, typeof(def_CBaseQualities__InqBool)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqBool(int CBaseQualities, int stype, bool* retval); // int __thiscall CBaseQualities::InqBool(CBaseQualities *this, unsigned int stype, int *retval)

        internal static int Call_CBaseQualities__InqFloat(int CBaseQualities, int stype, double* retval, int raw) => ((def_CBaseQualities__InqFloat)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590CD0, typeof(def_CBaseQualities__InqFloat)))(CBaseQualities, stype, retval, raw);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqFloat(int CBaseQualities, int stype, double* retval, int raw); // int __thiscall CBaseQualities::InqFloat(CBaseQualities *this, unsigned int stype, long double *retval, int raw)

        internal static int Call_CBaseQualities__InqDataID(int CBaseQualities, int stype, uint* retval) => ((def_CBaseQualities__InqDataID)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590D20, typeof(def_CBaseQualities__InqDataID)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqDataID(int CBaseQualities, int stype, uint* retval); // int __thiscall CBaseQualities::InqDataID(CBaseQualities *this, unsigned int stype, IDClass<_tagDataID,32,0> *retval)

        internal static int Call_CBaseQualities__InqInstanceID(int CBaseQualities, int stype, uint* retval) => ((def_CBaseQualities__InqInstanceID)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00590D50, typeof(def_CBaseQualities__InqInstanceID)))(CBaseQualities, stype, retval);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CBaseQualities__InqInstanceID(int CBaseQualities, int stype, uint* retval); // int __thiscall CBaseQualities::InqInstanceID(CBaseQualities *this, unsigned int stype, unsigned int *retval)

        internal static void Call_UIFlow__UseNewMode(int UIFlow) => ((def_UIFlow__UseNewMode)Marshal.GetDelegateForFunctionPointer(UIFlow__UseNewMode.Entrypoint, typeof(def_UIFlow__UseNewMode)))(UIFlow);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_UIFlow__UseNewMode(int UIFlow); // void __thiscall UIFlow::UseNewMode(UIFlow *this)
        internal static Hooker UIFlow__UseNewMode = new Hooker(0x00479AA0, 0x00479BB4);

        internal static void Call_Device__DoFrameSleep() => ((def_Device__DoFrameSleep)Marshal.GetDelegateForFunctionPointer(Device__DoFrameSleep.Entrypoint, typeof(def_Device__DoFrameSleep)))();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_Device__DoFrameSleep(); // void __cdecl Device::DoFrameSleep()
        internal static Hooker Device__DoFrameSleep = new Hooker(0x004392B0, 0x00412015);

        internal static void Call_SmartBox__Draw(int SmartBox, int a2, int a3) => ((def_SmartBox__Draw)Marshal.GetDelegateForFunctionPointer(SmartBox__Draw.Entrypoint, typeof(def_SmartBox__Draw)))(SmartBox, a2, a3);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__Draw(int SmartBox, int a2, int a3); // ???? not in source
        internal static Hooker SmartBox__Draw = new Hooker(0x00455610, 0x00412006);

        internal static int Call_UIElement__GetChildRecursive(int UIElement, int _ID) => ((def_UIElement__GetChildRecursive)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00463C00, typeof(def_UIElement__GetChildRecursive)))(UIElement, _ID);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_UIElement__GetChildRecursive(int UIElement, int _ID); // UIElement *__thiscall UIElement::GetChildRecursive(UIElement *this, unsigned int _ID)

        internal static void Call_UIElement__MoveTo(int UIElement, int x, int y) => ((def_UIElement__MoveTo)Marshal.GetDelegateForFunctionPointer((IntPtr)0x004634C0, typeof(def_UIElement__MoveTo)))(UIElement, x, y);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_UIElement__MoveTo(int UIElement, int x, int y); // void __thiscall UIElement::MoveTo(UIElement*this, const int _x, const int _y)

        internal static void Call_UIElement__ResizeTo(int UIElement, int width, int height) => ((def_UIElement__ResizeTo)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00463D60, typeof(def_UIElement__ResizeTo)))(UIElement, width, height);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_UIElement__ResizeTo(int UIElement, int width, int height); // void __userpurge UIElement::ResizeTo(UIElement *this@<ecx>, int a2@<edi>, const int _width, const int _height)

        internal static int Call_UIRegion__GetX(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE00, typeof(def_UIRegion_Get)))(UIRegion); // unsigned int __thiscall UIRegion::GetScreenX0(UIRegion *this)    
        internal static int Call_UIRegion__GetY(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE30, typeof(def_UIRegion_Get)))(UIRegion); // unsigned int __thiscall UIRegion::GetScreenY0(UIRegion *this)    
        internal static int Call_UIRegion__GetWidth(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE60, typeof(def_UIRegion_Get)))(UIRegion); // int __thiscall UIRegion::GetWidth(UIRegion *this)                
        internal static int Call_UIRegion__GetHeight(int UIRegion) => ((def_UIRegion_Get)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0069FE70, typeof(def_UIRegion_Get)))(UIRegion); // int __thiscall UIRegion::GetHeight(UIRegion *this)               
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_UIRegion_Get(int UIRegion);

        internal static void Call_SmartBox__PlayerPositionUpdated(double a2, int teleporting, float distance_moved) => ((def_SmartBox__PlayerPositionUpdated)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00453910, typeof(def_SmartBox__PlayerPositionUpdated)))(*Smartbox, a2, teleporting, distance_moved);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_SmartBox__PlayerPositionUpdated(int SmartBox, double a2, int teleporting, float distance_moved); // void __userpurge SmartBox::PlayerPositionUpdated(SmartBox *this, double a2, int teleporting, float distance_moved)

        internal static char Call_CM_UI__SendNotice_PlayerDescReceived(int CACQualities, int CPlayerModule) => ((def_CM_UI__SendNotice_PlayerDescReceived)Marshal.GetDelegateForFunctionPointer(CM_UI__SendNotice_PlayerDescReceived.Entrypoint, typeof(def_CM_UI__SendNotice_PlayerDescReceived)))(CACQualities, CPlayerModule);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_UI__SendNotice_PlayerDescReceived(int CACQualities, int CPlayerModule); // char __cdecl CM_UI::SendNotice_PlayerDescReceived(CACQualities *i_playerDesc, CPlayerModule *i_playerModule)
        internal static Hooker CM_UI__SendNotice_PlayerDescReceived = new Hooker(0x0047A200, 0x00564606);

        internal static void Call_CPlayerSystem__SetLogOffStarted(int CPlayerSystem) => ((def_CPlayerSystem__SetLogOffStarted)Marshal.GetDelegateForFunctionPointer(CPlayerSystem__SetLogOffStarted.Entrypoint, typeof(def_CPlayerSystem__SetLogOffStarted)))(CPlayerSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerSystem__SetLogOffStarted(int CPlayerSystem); // void __thiscall CPlayerSystem::SetLogOffStarted(CPlayerSystem *this)
        internal static Hooker CPlayerSystem__SetLogOffStarted = new Hooker(0x0055E4F0, 0x004D7AF3);

        internal static void Call_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, string message) => ((def_AC1Legacy__PStringBase_char__PStringBase_char)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0048C3E0, typeof(def_AC1Legacy__PStringBase_char__PStringBase_char)))(s_NullBuffer, message);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_AC1Legacy__PStringBase_char__PStringBase_char(int s_NullBuffer, [MarshalAs(UnmanagedType.LPStr)] string message); // void __thiscall AC1Legacy::PStringBase<char>::PStringBase<char>(AC1Legacy::PStringBase<char> *this, const char *str)




        internal static bool Call_CPhysicsObj__set_heading(int CPhysicsObj, float degrees, int send_event) => ((def_CPhysicsObj__set_heading)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00514C60, typeof(def_CPhysicsObj__set_heading)))(CPhysicsObj, degrees, send_event);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate bool def_CPhysicsObj__set_heading(int CPhysicsObj, float degrees, int send_event); //void __thiscall CPhysicsObj::set_heading(CPhysicsObj *this, float degrees, int send_event)

        internal static void Call_CMotionInterp__HitGround(int CMotionInterp) => ((def_CMotionInterp__HitGround)Marshal.GetDelegateForFunctionPointer(CMotionInterp__HitGround.Entrypoint, typeof(def_CMotionInterp__HitGround)))(CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CMotionInterp__HitGround(int CMotionInterp); // void __thiscall CMotionInterp::HitGround(CMotionInterp *this)
        internal static Hooker CMotionInterp__HitGround = new Hooker(0x005296D0, 0x00524F09);

        internal static void Call_CMotionInterp__LeaveGround(int CMotionInterp) => ((def_CMotionInterp__LeaveGround)Marshal.GetDelegateForFunctionPointer(CMotionInterp__LeaveGround.Entrypoint, typeof(def_CMotionInterp__LeaveGround)))(CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CMotionInterp__LeaveGround(int CMotionInterp); // void __thiscall CMotionInterp::LeaveGround(CMotionInterp *this)
        internal static Hooker CMotionInterp__LeaveGround = new Hooker(0x00529710, 0x00524F29);

        internal static double Call_CMotionInterp__get_max_speed() => ((def_CMotionInterp__get_max_speed)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005288C0, typeof(def_CMotionInterp__get_max_speed)))(CMotionInterp);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate double def_CMotionInterp__get_max_speed(int CMotionInterp); // double __thiscall CMotionInterp::get_max_speed(CMotionInterp *this)

        internal static void Call_CPlayerSystem__Handle_Login__CharacterSet(int CPlayerSystem, int buff, uint size) => ((def_CPlayerSystem__Handle_Login__CharacterSet)Marshal.GetDelegateForFunctionPointer(CPlayerSystem__Handle_Login__CharacterSet.Entrypoint, typeof(def_CPlayerSystem__Handle_Login__CharacterSet)))(CPlayerSystem, buff, size);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerSystem__Handle_Login__CharacterSet(int CPlayerSystem, int buff, uint size); // void __thiscall CPlayerSystem::Handle_Login__CharacterSet(CPlayerSystem *this, void *buff, unsigned int size)
        internal static Hooker CPlayerSystem__Handle_Login__CharacterSet = new Hooker(0x00560440, 0x0055D68A);

        internal static int Call_ClientUISystem__Handle_Character__ConfirmationRequest(int confirm, int context, int userData) => ((def_ClientUISystem__Handle_Character__ConfirmationRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00564E40, typeof(def_ClientUISystem__Handle_Character__ConfirmationRequest)))(confirm, context, userData);
        [UnmanagedFunctionPointer(CallingConvention.StdCall)] internal delegate int def_ClientUISystem__Handle_Character__ConfirmationRequest(int confirm, int context, int userData); // unsigned int __stdcall ClientUISystem::Handle_Character__ConfirmationRequest(int confirm, unsigned int context, AC1Legacy::PStringBase<char> *userData)
        internal static Hooker ClientUISystem__Handle_Character__ConfirmationRequest = new Hooker(0x00564E40, 0x006A3C25);

        internal static void Call_ClientUISystem_IncrementBusyCount() => ((def_ClientUISystem_IncrementBusyCount)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00565610, typeof(def_ClientUISystem_IncrementBusyCount)))(*ClientUISystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientUISystem_IncrementBusyCount(int ClientUISystem); // void __thiscall ClientUISystem::IncrementBusyCount(ClientUISystem *this)

        internal static void Call_ClientUISystem_DecrementBusyCount() => ((def_ClientUISystem_DecrementBusyCount)Marshal.GetDelegateForFunctionPointer((IntPtr)0x00565630, typeof(def_ClientUISystem_DecrementBusyCount)))(*ClientUISystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientUISystem_DecrementBusyCount(int ClientUISystem); // void __thiscall ClientUISystem::DecrementBusyCount(ClientUISystem *this)

        internal static void Call_ClientCombatSystem__CommenceJump() => ((def_ClientCombatSystem__CommenceJump)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056BCD0, typeof(def_ClientCombatSystem__CommenceJump)))(clientCombatSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__CommenceJump(int* clientCombatSystem); // void __thiscall ClientCombatSystem::CommenceJump(ClientCombatSystem*this)

        internal static void Call_ClientCombatSystem__DoJump(int autonomous) => ((def_ClientCombatSystem__DoJump)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056BE50, typeof(def_ClientCombatSystem__DoJump)))(clientCombatSystem, autonomous);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__DoJump(int* clientCombatSystem, int autonomous); // void __thiscall ClientCombatSystem::DoJump(ClientCombatSystem *this, bool autonomous)

        internal static int Call_ClientCombatSystem__GetDefaultCombatMode(bool _quiet) => ((def_ClientCombatSystem__GetDefaultCombatMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056C050, typeof(def_ClientCombatSystem__GetDefaultCombatMode)))(clientCombatSystem, _quiet);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_ClientCombatSystem__GetDefaultCombatMode(int* clientCombatSystem, bool _quiet); //.text:0056C050 signed int __thiscall ClientCombatSystem::GetDefaultCombatMode(ClientCombatSystem *this, bool _quiet)

        internal static void Call_ClientCombatSystem__ExecuteAttack(ATTACK_HEIGHT _attackHeight, int _expectServerResponse) => ((def_ClientCombatSystem__ExecuteAttack)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056C8C0, typeof(def_ClientCombatSystem__ExecuteAttack)))(clientCombatSystem, _attackHeight, _expectServerResponse);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__ExecuteAttack(int* clientCombatSystem, ATTACK_HEIGHT _attackHeight, int _expectServerResponse); //.text:0056C8C0 void __thiscall ClientCombatSystem::ExecuteAttack(ClientCombatSystem *this, ATTACK_HEIGHT _attackHeight, bool _expectServerResponse)

        internal static void Call_ClientCombatSystem__SetCombatMode(COMBAT_MODE i_NewCombatMode, bool i_bPlayerRequested) => ((def_ClientCombatSystem__SetCombatMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056CB80, typeof(def_ClientCombatSystem__SetCombatMode)))(clientCombatSystem, i_NewCombatMode, i_bPlayerRequested);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__SetCombatMode(int* clientCombatSystem, COMBAT_MODE i_NewCombatMode, bool i_bPlayerRequested); //.text:0056CB80 void __thiscall ClientCombatSystem::SetCombatMode(ClientCombatSystem *this, COMBAT_MODE i_NewCombatMode, bool i_bPlayerRequested)

        internal static void Call_ClientCombatSystem__StartAttackRequest() => ((def_ClientCombatSystem__StartAttackRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056CD90, typeof(def_ClientCombatSystem__StartAttackRequest)))(clientCombatSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__StartAttackRequest(int* clientCombatSystem); //.text:0056CD90 void __thiscall ClientCombatSystem::StartAttackRequest(ClientCombatSystem *this)

        internal static void Call_ClientCombatSystem__EndAttackRequest(ATTACK_HEIGHT _attackHeight, float _power) => ((def_ClientCombatSystem__EndAttackRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056CE30, typeof(def_ClientCombatSystem__EndAttackRequest)))(clientCombatSystem, _attackHeight, _power);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__EndAttackRequest(int* clientCombatSystem, ATTACK_HEIGHT _attackHeight, float _power); //.text:0056CE30 void __thiscall ClientCombatSystem::EndAttackRequest(ClientCombatSystem *this, ATTACK_HEIGHT _attackHeight, float _power)

        internal static void Call_ClientCombatSystem__ToggleCombatMode() => ((def_ClientCombatSystem__ToggleCombatMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056D610, typeof(def_ClientCombatSystem__ToggleCombatMode)))(clientCombatSystem);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__ToggleCombatMode(int* clientCombatSystem); //.text:0056D610 void __thiscall ClientCombatSystem::ToggleCombatMode(ClientCombatSystem *this)

        internal static void Call_ClientCombatSystem__SetRequestedAttackHeight(ATTACK_HEIGHT _height) => ((def_ClientCombatSystem__SetRequestedAttackHeight)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0056D640, typeof(def_ClientCombatSystem__SetRequestedAttackHeight)))(clientCombatSystem, _height);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_ClientCombatSystem__SetRequestedAttackHeight(int* clientCombatSystem, ATTACK_HEIGHT _height); //.text:0056D640 void __thiscall ClientCombatSystem::SetRequestedAttackHeight(ClientCombatSystem *this, ATTACK_HEIGHT _height)

        internal static void Call_CPlayerModule__SaveToServer(bool i_bForceUpdate) => ((def_CPlayerModule__SaveToServer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x0059B670, typeof(def_CPlayerModule__SaveToServer)))(*CPlayerModule + 0x30, i_bForceUpdate);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_CPlayerModule__SaveToServer(int CPlayerModule, bool i_bForceUpdate); // void __thiscall CPlayerModule::SaveToServer(CPlayerModule *this, bool i_bForceUpdate)

        internal static bool Call_PlayerModule__GetOption(int i_eOption) => ((def_PlayerModule__GetOption)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005D4AF0, typeof(def_PlayerModule__GetOption)))((*CPlayerModule) + 0x34, i_eOption);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate bool def_PlayerModule__GetOption(int playermodule, int i_eOption); // bool __thiscall PlayerModule::GetOption(PlayerModule *this, PlayerOption po)

        internal static void Call_PlayerModule__SetOption(int option, bool on) => ((def_PlayerModule__SetOption)Marshal.GetDelegateForFunctionPointer((IntPtr)0x005D4F20, typeof(def_PlayerModule__SetOption)))((*P.CPlayerModule) + 0x34, option, on);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void def_PlayerModule__SetOption(int playermodule, int i_eOption, bool on); // void __thiscall PlayerModule::SetOption(PlayerModule *this, PlayerOption po, bool on)

        internal static IntPtr Entrypoint_CM_Character__Event_AddShortCut = (IntPtr)0x006A1CD0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_AddShortCut(int CShortCutData); // bool __cdecl CM_Character::Event_AddShortCut(CShortCutData *i_scData)

        internal static IntPtr Entrypoint_CM_Character__Event_AddSpellFavorite = (IntPtr)0x006A1D90;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_AddSpellFavorite(int i_spid, int i_index, int i_list); // bool __cdecl CM_Character::Event_AddSpellFavorite(unsigned int i_spid, int i_index, int i_list)

        internal static bool Call_CM_Character__Event_ConfirmationResponse(int i_confirmType, int i_context, int i_bAccepted) => ((def_CM_Character__Event_ConfirmationResponse)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A2030, typeof(def_CM_Character__Event_ConfirmationResponse)))(i_confirmType, i_context, i_bAccepted);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_ConfirmationResponse(int i_confirmType, int i_context, int i_bAccepted); // bool __cdecl CM_Character::Event_ConfirmationResponse(int i_confirmType, unsigned int i_context, int i_bAccepted)

        internal static bool Call_CM_Character__Event_LoginCompleteNotification() => ((def_CM_Character__Event_LoginCompleteNotification)Marshal.GetDelegateForFunctionPointer(CM_Character__Event_LoginCompleteNotification.Entrypoint, typeof(def_CM_Character__Event_LoginCompleteNotification)))();
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_LoginCompleteNotification(); // bool __cdecl CM_Character::Event_LoginCompleteNotification()
        internal static Hooker CM_Character__Event_LoginCompleteNotification = new Hooker(0x006A22A0, 0x00563C54);

        internal static IntPtr Entrypoint_CM_Character__Event_PlayerOptionChangedEvent = (IntPtr)0x006A2330;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Character__Event_PlayerOptionChangedEvent(int i_eOption, int i_value); // bool __cdecl CM_Character::Event_PlayerOptionChangedEvent(PlayerOption i_po, int i_value)

        internal static IntPtr Entrypoint_CM_Character__Event_RemoveShortCut = (IntPtr)0x006A25E0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_RemoveShortCut(int i_index); // bool __cdecl CM_Character::Event_RemoveShortCut(int i_index)

        internal static IntPtr Entrypoint_CM_Character__Event_RemoveSpellFavorite = (IntPtr)0x006A26B0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_RemoveSpellFavorite(int i_spid, int i_list); // bool __cdecl CM_Character::Event_RemoveSpellFavorite(unsigned int i_spid, int i_list)

        internal static IntPtr Entrypoint_CM_Character__Event_SpellbookFilterEvent = (IntPtr)0x006A2850;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_SpellbookFilterEvent(int i_options); // bool __cdecl CM_Character::Event_SpellbookFilterEvent(unsigned int i_options)

        internal static IntPtr Entrypoint_CM_Character__Event_Suicide = (IntPtr)0x006A2920;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_Suicide(); // bool __cdecl CM_Character::Event_Suicide()

        internal static IntPtr Entrypoint_CM_Character__Event_TeleToLifestone = (IntPtr)0x006A29B0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_TeleToLifestone(); // bool __cdecl CM_Character::Event_TeleToLifestone()

        internal static IntPtr Entrypoint_CM_Character__Event_TeleToMarketplace = (IntPtr)0x006A2A40;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_TeleToMarketplace(); // bool __cdecl CM_Character::Event_TeleToMarketplace()

        internal static IntPtr Entrypoint_CM_Character__Event_TeleToPKArena = (IntPtr)0x006A2AD0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_TeleToPKArena(); // bool __cdecl CM_Character::Event_TeleToPKArena()

        internal static IntPtr Entrypoint_CM_Character__Event_TeleToPKLArena = (IntPtr)0x006A2B60;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_TeleToPKLArena(); // bool __cdecl CM_Character::Event_TeleToPKLArena()

        internal static IntPtr Entrypoint_CM_Character__Event_SetDesiredComponentLevel = (IntPtr)0x006A3170;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Character__Event_SetDesiredComponentLevel(int i_wcid, int i_amount); // bool __cdecl CM_Character::Event_SetDesiredComponentLevel(IDClass<_tagDataID,32,0> i_wcid, int i_amount)

        internal static char Call_CM_Character__SendNotice_SetPowerbarLevel(Jumper.PowerBarMode i_pbm, float i_fLevel) => ((def_CM_Character__SendNotice_SetPowerbarLevel)Marshal.GetDelegateForFunctionPointer(CM_Character__SendNotice_SetPowerbarLevel.Entrypoint, typeof(def_CM_Character__SendNotice_SetPowerbarLevel)))(i_pbm, i_fLevel);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Character__SendNotice_SetPowerbarLevel(Jumper.PowerBarMode i_pbm, float i_fLevel); // char __cdecl CM_Character::SendNotice_SetPowerbarLevel(PowerBarMode i_pbm, float i_fLevel)
        internal static Hooker CM_Character__SendNotice_SetPowerbarLevel = new Hooker(0x006A36D0, 0x0056B5DC);

        internal static IntPtr Entrypoint_CM_Magic__Event_CastTargetedSpell = (IntPtr)0x006A3E60;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Magic__Event_CastTargetedSpell(int i_target, int i_spell_id); // bool __cdecl CM_Magic::Event_CastTargetedSpell(unsigned int i_target, unsigned int i_spell_id)

        internal static IntPtr Entrypoint_CM_Magic__Event_CastUntargetedSpell = (IntPtr)0x006A3F70;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Magic__Event_CastUntargetedSpell(int i_spell_id); // bool __cdecl CM_Magic::Event_CastUntargetedSpell(unsigned int i_spell_id)

        internal static IntPtr Entrypoint_CM_Communication__Event_ChannelBroadcast = (IntPtr)0x006A4E50;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_ChannelBroadcast(int i_chan, int i_msg); // bool __cdecl CM_Communication::Event_ChannelBroadcast(unsigned int i_chan, AC1Legacy::PStringBase<char> *i_msg)

        internal static IntPtr Entrypoint_CM_Communication__Event_Emote = (IntPtr)0x006A4F40;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_Emote(int i_msg); // bool __cdecl CM_Communication::Event_Emote(AC1Legacy::PStringBase<char> *i_msg)

        internal static IntPtr Entrypoint_CM_Communication__Event_SoulEmote = (IntPtr)0x006A5320;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_SoulEmote(int i_msg); // bool __cdecl CM_Communication::Event_SoulEmote(AC1Legacy::PStringBase<char> *i_msg)

        internal static IntPtr Entrypoint_CM_Communication__Event_Talk = (IntPtr)0x006A53E0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_Talk(int i_msg); // bool __cdecl CM_Communication::Event_Talk(AC1Legacy::PStringBase<char> *i_msg)

        internal static bool Call_CM_Communication__Event_TalkDirect(int i_msg, int i_target_id) => ((def_CM_Communication__Event_TalkDirect)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A54A0, typeof(def_CM_Communication__Event_TalkDirect)))(i_msg, i_target_id);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_TalkDirect(int i_msg, int i_target_id); // bool __cdecl CM_Communication::Event_TalkDirect(AC1Legacy::PStringBase<char> *i_msg, unsigned int i_target_id)

        internal static IntPtr Entrypoint_CM_Communication__Event_TalkDirectByName = (IntPtr)0x006A55A0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Communication__Event_TalkDirectByName(int i_msg, int i_target_name); // bool __cdecl CM_Communication::Event_TalkDirectByName(AC1Legacy::PStringBase<char> *i_msg, AC1Legacy::PStringBase<char> *i_target_name)

        internal static bool Call_CM_Fellowship__Event_AssignNewLeader(int i_target) => ((def_CM_Fellowship__Event_AssignNewLeader)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A6ED0, typeof(def_CM_Fellowship__Event_AssignNewLeader)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_AssignNewLeader(int i_target); // bool __cdecl CM_Fellowship::Event_AssignNewLeader(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_ChangeFellowOpeness(int i_open) => ((def_CM_Fellowship__Event_ChangeFellowOpeness)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A6FA0, typeof(def_CM_Fellowship__Event_ChangeFellowOpeness)))(i_open);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_ChangeFellowOpeness(int i_open); // bool __cdecl CM_Fellowship::Event_ChangeFellowOpeness(int i_open)

        internal static bool Call_CM_Fellowship__Event_Dismiss(int i_target) => ((def_CM_Fellowship__Event_Dismiss)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7070, typeof(def_CM_Fellowship__Event_Dismiss)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Dismiss(int i_target); // bool __cdecl CM_Fellowship::Event_Dismiss(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_Quit(int i_disband) => ((def_CM_Fellowship__Event_Quit)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7140, typeof(def_CM_Fellowship__Event_Quit)))(i_disband);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Quit(int i_disband); // bool __cdecl CM_Fellowship::Event_Quit(int i_disband)

        internal static bool Call_CM_Fellowship__Event_Recruit(int i_target) => ((def_CM_Fellowship__Event_Recruit)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7210, typeof(def_CM_Fellowship__Event_Recruit)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Recruit(int i_target); // bool __cdecl CM_Fellowship::Event_Recruit(unsigned int i_target)

        internal static bool Call_CM_Fellowship__Event_UpdateRequest(int i_on) => ((def_CM_Fellowship__Event_UpdateRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A72E0, typeof(def_CM_Fellowship__Event_UpdateRequest)))(i_on);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_UpdateRequest(int i_on); // bool __cdecl CM_Fellowship::Event_UpdateRequest(int i_on)

        internal static bool Call_CM_Fellowship__Event_Create(int i_name, int i_share_xp) => ((def_CM_Fellowship__Event_Create)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A7700, typeof(def_CM_Fellowship__Event_Create)))(i_name, i_share_xp);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Fellowship__Event_Create(int i_name, int i_share_xp); // bool __cdecl CM_Fellowship::Event_Create(AC1Legacy::PStringBase<char> *i_name, int i_share_xp)

        internal static bool Call_CM_Allegiance__Event_BreakAllegiance(int i_target) => ((def_CM_Allegiance__Event_BreakAllegiance)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A78E0, typeof(def_CM_Allegiance__Event_BreakAllegiance)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_BreakAllegiance(int i_target); // bool __cdecl CM_Allegiance::Event_BreakAllegiance(unsigned int i_target)

        internal static IntPtr Entrypoint_CM_Allegiance__Event_RecallAllegianceHometown = (IntPtr)0x006A8060;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_RecallAllegianceHometown(); // bool __cdecl CM_Allegiance::Event_RecallAllegianceHometown()

        internal static bool Call_CM_Allegiance__Event_SwearAllegiance(int i_target) => ((def_CM_Allegiance__Event_SwearAllegiance)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006A80F0, typeof(def_CM_Allegiance__Event_SwearAllegiance)))(i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_SwearAllegiance(int i_target); // bool __cdecl CM_Allegiance::Event_SwearAllegiance(unsigned int i_target)

        internal static IntPtr Entrypoint_CM_Allegiance__Event_UpdateRequest = (IntPtr)0x006A81C0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Allegiance__Event_UpdateRequest(int i_on); // bool __cdecl CM_Allegiance::Event_UpdateRequest(int i_on)

        internal static IntPtr Entrypoint_CM_Train__Event_TrainAttribute = (IntPtr)0x006A8E90;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Train__Event_TrainAttribute(int i_atype, int i_xp_spent); // bool __cdecl CM_Train__Event_TrainAttribute(int i_atype, int i_xp_spent);

        internal static IntPtr Entrypoint_CM_Train__Event_TrainAttribute2nd = (IntPtr)0x006A8FA0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Train__Event_TrainAttribute2nd(int i_atype, int i_xp_spent); // bool __cdecl CM_Train__Event_TrainAttribute2nd(int i_atype, int i_xp_spent);

        internal static IntPtr Entrypoint_CM_Train__Event_TrainSkill = (IntPtr)0x006A90B0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Train__Event_TrainSkill(int i_stype, int i_xp_spent); // bool __cdecl CM_Train__Event_TrainSkill(int i_stype, int i_xp_spent);

        internal static IntPtr Entrypoint_CM_Train__Event_TrainSkillAdvancementClass = (IntPtr)0x006A91C0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Train__Event_TrainSkillAdvancementClass(int i_stype, int i_credits_spent); // bool __cdecl CM_Train__Event_TrainSkillAdvancementClass(int i_stype, int i_credits_spent);

        internal static IntPtr Entrypoint_CM_Item__Event_Appraise = (IntPtr)0x006A94A0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Item__Event_Appraise(int i_objectID); // bool __cdecl CM_Item::Event_Appraise(unsigned int i_objectID)

        internal static IntPtr Entrypoint_CM_Vendor__Event_Sell = (IntPtr)0x006AAF60;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Vendor__Event_Sell(int i_vendorID, int i_stuff); // bool __cdecl CM_Vendor::Event_Sell(unsigned int i_vendorID, PackableList<ItemProfile> *i_stuff)

        internal static IntPtr Entrypoint_CM_Vendor__Event_Buy = (IntPtr)0x006AB050;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate void def_CM_Vendor__Event_Buy(int vendorID, int* PackableList_ItemProfile, int i_alternateCurrencyID); // bool __cdecl CM_Vendor::Event_Buy(unsigned int i_vendorID, PackableList<ItemProfile> *i_stuff, IDClass<_tagDataID,32,0> i_alternateCurrencyID)

        internal static char Call_CM_Vendor__SendNotice_CloseVendor(int i_bUpdating) => ((def_CM_Vendor__SendNotice_CloseVendor)Marshal.GetDelegateForFunctionPointer(CM_Vendor__SendNotice_CloseVendor.Entrypoint, typeof(def_CM_Vendor__SendNotice_CloseVendor)))(i_bUpdating);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Vendor__SendNotice_CloseVendor(int i_bUpdating); // char __cdecl CM_Vendor::SendNotice_CloseVendor(int i_bUpdating)
        internal static Hooker CM_Vendor__SendNotice_CloseVendor = new Hooker(0x006AB1D0, 0x00564E27);

        internal static char Call_CM_Vendor__SendNotice_OpenVendor(int i_vendorID, int i_vendorProfile, int i_itemProfileList, int i_startMode) => ((def_CM_Vendor__SendNotice_OpenVendor)Marshal.GetDelegateForFunctionPointer(CM_Vendor__SendNotice_OpenVendor.Entrypoint, typeof(def_CM_Vendor__SendNotice_OpenVendor)))(i_vendorID, i_vendorProfile, i_itemProfileList, i_startMode);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate char def_CM_Vendor__SendNotice_OpenVendor(int i_vendorID, int i_vendorProfile, int i_itemProfileList, int i_startMode); // char __cdecl CM_Vendor::SendNotice_OpenVendor(unsigned int i_vendorID, VendorProfile *i_vendorProfile, PackableList<ItemProfile> *i_itemProfileList, ShopMode i_startMode)
        internal static Hooker CM_Vendor__SendNotice_OpenVendor = new Hooker(0x006AB220, 0x00566B3C);

        internal static IntPtr Entrypoint_CM_House__Event_TeleToHouse_Event = (IntPtr)0x006ABF00;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_House__Event_TeleToHouse_Event(); // bool __cdecl CM_House::Event_TeleToHouse_Event()

        internal static IntPtr Entrypoint_CM_House__Event_TeleToMansion_Event = (IntPtr)0x006ABF90;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_House__Event_TeleToMansion_Event(); // bool __cdecl CM_House::Event_TeleToMansion_Event()

        internal static IntPtr Entrypoint_CM_Inventory__Event_CreateTinkeringTool = (IntPtr)0x006AC790;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_CreateTinkeringTool(int i_toolID, int i_gems); // bool __cdecl CM_Inventory::Event_CreateTinkeringTool(unsigned int i_toolID, PackableList<unsigned long> *i_gems)

        internal static bool Call_CM_Inventory__Event_DropItem(int i_item) => ((def_CM_Inventory__Event_DropItem)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AC880, typeof(def_CM_Inventory__Event_DropItem)))(i_item);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_DropItem(int i_item); // bool __cdecl CM_Inventory::Event_DropItem(unsigned int i_item)

        internal static IntPtr Entrypoint_CM_Inventory__Event_GetAndWieldItem = (IntPtr)0x006AC950;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_GetAndWieldItem(int i_item, int i_loc); // bool __cdecl CM_Inventory::Event_GetAndWieldItem(unsigned int i_item, unsigned int i_loc)

        internal static bool Call_CM_Inventory__Event_GiveObjectRequest(int i_targetID, int i_objectID, int i_amount) => ((def_CM_Inventory__Event_GiveObjectRequest)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACA60, typeof(def_CM_Inventory__Event_GiveObjectRequest)))(i_targetID, i_objectID, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_GiveObjectRequest(int i_targetID, int i_objectID, int i_amount); // bool __cdecl CM_Inventory::Event_GiveObjectRequest(unsigned int i_targetID, unsigned int i_objectID, unsigned int i_amount)

        internal static IntPtr Entrypoint_CM_Inventory__Event_NoLongerViewingContents = (IntPtr)0x006ACBB0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_NoLongerViewingContents(int i_container); // bool __cdecl CM_Inventory::Event_NoLongerViewingContents(unsigned int i_container)

        internal static bool Call_CM_Inventory__Event_PutItemInContainer(int i_item, int i_container, int i_loc) => ((def_CM_Inventory__Event_PutItemInContainer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACC80, typeof(def_CM_Inventory__Event_PutItemInContainer)))(i_item, i_container, i_loc);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_PutItemInContainer(int i_item, int i_container, int i_loc); // bool __cdecl CM_Inventory::Event_PutItemInContainer(unsigned int i_item, unsigned int i_container, unsigned int i_loc)

        internal static bool Call_CM_Inventory__Event_StackableMerge(int i_mergeFromID, int i_mergeToID, int i_amount) => ((def_CM_Inventory__Event_StackableMerge)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006ACDD0, typeof(def_CM_Inventory__Event_StackableMerge)))(i_mergeFromID, i_mergeToID, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableMerge(int i_mergeFromID, int i_mergeToID, int i_amount); // bool __cdecl CM_Inventory::Event_StackableMerge(unsigned int i_mergeFromID, unsigned int i_mergeToID, int i_amount)

        internal static IntPtr Entrypoint_CM_Inventory__Event_StackableSplitTo3D = (IntPtr)0x006ACF20;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableSplitTo3D(int i_stackID, int i_amount); // bool __cdecl CM_Inventory::Event_StackableSplitTo3D(unsigned int i_stackID, int i_amount)

        internal static bool Call_CM_Inventory__Event_StackableSplitToContainer(int i_stackID, int i_containerID, int i_place, int i_amount) => ((def_CM_Inventory__Event_StackableSplitToContainer)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD030, typeof(def_CM_Inventory__Event_StackableSplitToContainer)))(i_stackID, i_containerID, i_place, i_amount);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableSplitToContainer(int i_stackID, int i_containerID, int i_place, int i_amount); // bool __cdecl CM_Inventory::Event_StackableSplitToContainer(unsigned int i_stackID, unsigned int i_containerID, int i_place, int i_amount)

        internal static IntPtr Entrypoint_CM_Inventory__Event_StackableSplitToWield = (IntPtr)0x006AD1C0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_StackableSplitToWield(int i_stackID, int i_loc, int i_amount); // bool __cdecl CM_Inventory::Event_StackableSplitToWield(unsigned int i_stackID, unsigned int i_loc, int i_amount)

        internal static bool Call_CM_Inventory__Event_UseEvent(int i_object) => ((def_CM_Inventory__Event_UseEvent)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD310, typeof(def_CM_Inventory__Event_UseEvent)))(i_object);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_UseEvent(int i_object); // bool __cdecl CM_Inventory::Event_UseEvent(unsigned int i_object)

        internal static bool Call_CM_Inventory__Event_UseWithTargetEvent(int i_object, int i_target) => ((def_CM_Inventory__Event_UseWithTargetEvent)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AD3E0, typeof(def_CM_Inventory__Event_UseWithTargetEvent)))(i_object, i_target);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Inventory__Event_UseWithTargetEvent(int i_object, int i_target); // bool __cdecl CM_Inventory::Event_UseWithTargetEvent(unsigned int i_object, unsigned int i_target)

        internal static IntPtr Entrypoint_CM_Trade__Event_AcceptTrade = (IntPtr)0x006ADF70;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_AcceptTrade(int i_stuff); // bool __cdecl CM_Trade::Event_AcceptTrade(Trade *i_stuff)

        internal static IntPtr Entrypoint_CM_Trade__Event_AddToTrade = (IntPtr)0x006AE030;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_AddToTrade(int i_item, int i_loc); // bool __cdecl CM_Trade::Event_AddToTrade(unsigned int i_item, unsigned int i_loc)

        internal static IntPtr Entrypoint_CM_Trade__Event_CloseTradeNegotiations = (IntPtr)0x006AE140;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_CloseTradeNegotiations(); // bool __cdecl CM_Trade::Event_CloseTradeNegotiations()

        internal static IntPtr Entrypoint_CM_Trade__Event_DeclineTrade = (IntPtr)0x006AE1D0;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_DeclineTrade(); // bool __cdecl CM_Trade::Event_DeclineTrade()

        internal static IntPtr Entrypoint_CM_Trade__Event_OpenTradeNegotiations = (IntPtr)0x006AE260;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_OpenTradeNegotiations(int i_other); // bool __cdecl CM_Trade::Event_OpenTradeNegotiations(unsigned int i_other)

        internal static IntPtr Entrypoint_CM_Trade__Event_ResetTrade = (IntPtr)0x006AE330;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Trade__Event_ResetTrade(); // bool __cdecl CM_Trade::Event_ResetTrade()

        internal static IntPtr Entrypoint_CM_Movement__Event_Jump_NonAutonomous = (IntPtr)0x006B0A00;
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate bool def_CM_Movement__Event_Jump_NonAutonomous(float i_extent); // bool __cdecl CM_Movement::Event_Jump_NonAutonomous(float i_extent)

        internal static IntPtr Entrypoint_CommandInterpreter__SetHoldSidestep = (IntPtr)0x006B4350;
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate bool def_CommandInterpreter__SetHoldSidestep(int CommandInterpreter, int new_value); // void __thiscall CommandInterpreter::SetHoldSidestep(CommandInterpreter *this, int new_value)


        //.text:006AA810 ; bool __cdecl CM_Writing::Event_SetInscription(unsigned long,class AC1Legacy::PStringBase<char> const &)
        //.text:006AA940 ; bool __cdecl CM_Combat::Event_CancelAttack(void)
        //.text:006AA9D0 ; bool __cdecl CM_Combat::Event_ChangeCombatMode(enum COMBAT_MODE)
        //.text:006AAAA0 ; bool __cdecl CM_Combat::Event_QueryHealth(unsigned long)


        // Notifies client UI to redraw the Attack Level Buttons
        internal static int Call_CM_Combat__SendNotice_AttackHeightChanged(ATTACK_HEIGHT i_i_height) => ((def_CM_Combat__SendNotice_AttackHeightChanged)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AAE10, typeof(def_CM_Combat__SendNotice_AttackHeightChanged)))(i_i_height);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_CM_Combat__SendNotice_AttackHeightChanged(ATTACK_HEIGHT i_i_height);

        // Notifies client UI to redraw the Attack Power slider
        internal static int Call_CM_Combat__SendNotice_DesiredAttackPowerChanged(float i_i_fLevel) => ((def_CM_Combat__SendNotice_DesiredAttackPowerChanged)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AAE60, typeof(def_CM_Combat__SendNotice_DesiredAttackPowerChanged)))(i_i_fLevel);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_CM_Combat__SendNotice_DesiredAttackPowerChanged(float i_i_fLevel); // char __cdecl CM_Combat::SendNotice_DesiredAttackPowerChanged(float i_i_fLevel)

        internal static int Call_CM_Combat__SendNotice_SetCombatMode(COMBAT_MODE i_eCombatMode) => ((def_CM_Combat__SendNotice_SetCombatMode)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AAEB0, typeof(def_CM_Combat__SendNotice_SetCombatMode)))(i_eCombatMode);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_CM_Combat__SendNotice_SetCombatMode(COMBAT_MODE i_eCombatMode);

        internal static int Call_CM_Combat__SendNotice_UpdateObjectHealth(int i_target, float i_health) => ((def_CM_Combat__SendNotice_UpdateObjectHealth)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006AAF00, typeof(def_CM_Combat__SendNotice_UpdateObjectHealth)))(i_target, i_health);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_CM_Combat__SendNotice_UpdateObjectHealth(int i_target, float i_health);





        internal static int Call_CommandInterpreter__TurnToHeading(float new_heading, int run) => ((def_CommandInterpreter__TurnToHeading)Marshal.GetDelegateForFunctionPointer((IntPtr)0x006B54B0, typeof(def_CommandInterpreter__TurnToHeading)))(CommandInterpreter, new_heading, run);
        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate int def_CommandInterpreter__TurnToHeading(int CommandInterpreter, float new_heading, int run); // int __thiscall CommandInterpreter::TurnToHeading(CommandInterpreter *this, float new_heading, int run)

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate char def_SmartArray_RadarInfo__AddToEnd(int SmartArray_RadarInfo, int i_rData); // char __thiscall SmartArray<RadarInfo,1>::AddToEnd(SmartArray<RadarInfo,1> *this, RadarInfo *i_rData)
        internal static Hooker SmartArray_RadarInfo__AddToEnd = new Hooker(0x004D9C60, 0x004DA621);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate char def_SmartArray_RadarInfo__RemoveUnOrdered(int SmartArray_RadarInfo, int i_rData); // char __thiscall SmartArray<RadarInfo,1>::RemoveUnOrdered(SmartArray<RadarInfo,1> *this, RadarInfo *i_rData)
        internal static Hooker SmartArray_RadarInfo__RemoveUnOrdered = new Hooker(0x004D8D50, 0x004D9054);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate char def_SmartArray_RadarInfo__grow(int SmartArray_RadarInfo, int i_nSize);// char __thiscall SmartArray<RadarInfo,1>::grow(SmartArray<RadarInfo,1> *this, unsigned int i_nSize)
        internal static Hooker SmartArray_RadarInfo__grow = new Hooker(0x004D9340, 0x004D9C7E);

        internal static int Call_Proto_UI__SendEnterWorld(int gid, int account) => ((def_Proto_UI__SendEnterWorld)Marshal.GetDelegateForFunctionPointer(Proto_UI__SendEnterWorld.Entrypoint, typeof(def_Proto_UI__SendEnterWorld)))(gid, account);
        [UnmanagedFunctionPointer(CallingConvention.Cdecl)] internal delegate int def_Proto_UI__SendEnterWorld(int gid, int account); // int __cdecl Proto_UI::SendEnterWorld(unsigned int gid, accountID account)
        internal static Hooker Proto_UI__SendEnterWorld = new Hooker(0x00547780, 0x0056069F);

        [UnmanagedFunctionPointer(CallingConvention.ThisCall)] internal delegate void __vecDelDtor(int obj, int a2); //no Entrypoint - comes from the object its' self.



        internal static double* Timer__cur_time = (double*)0x008379A8;
        internal static double* uptime = (double*)0x008379B0;
        internal static int* gmClient = (int*)0x008379E4;
        internal static byte* Device__m_bIsActiveApp = (byte*)0x00838197;
        internal static byte* Device__m_bTrackLeaveCalled = (byte*)0x0083819A;
        internal static int* Device__m_hWnd = (int*)0x008381A4;
        internal static int* GlobalEventHandler = (int*)0x00838374;
        internal static int* Smartbox = (int*)0x0083DA58;
        internal static int* UIElementManager = (int*)0x0083E03C;
        internal static int* UIFlow = (int*)0x0083E72C;
        internal static int* cObjectMaint = (int*)0x00842ADC;
        internal static int* ProtoUI__eventCounter_ = (int*)0x00846F38;
        internal static int* CPhysicsPart__player_iid = (int*)0x00844C08;
        internal static int* CPhysicsPart__player_object = (int*)0x00844D68;
        internal static int* PacketController = (int*)0x00846F08;
        internal static int* CPlayerModule = (int*)0x0087119C;
        internal static int* ClientUISystem = (int*)0x00871354;
        internal static int* ClientMagicSystem = (int*)0x0087144C;
        internal static int* ClientFellowshipSystem = (int*)0x0087150C;
        internal static int* ClientAllegianceSystem = (int*)0x008715BC;
        internal static int* clientCombatSystem = (int*)0x0087166C;
        internal static int* ClientTradeSystem = (int*)0x0087174C;
        internal static int* ClientCommunicationSystem = (int*)0x008717FC;
        internal static int* ClientMiniGameSystem = (int*)0x0087190C;
        internal static int* ClientHousingSystem = (int*)0x008719BC;
        internal static uint* ACCWeenieObject__selectedID = (uint*)0x00871E54;
        internal static uint* ACCWeenieObject__prevSelectedID = (uint*)0x00871E58;
        internal static uint* ACCWeenieObject__prevSelectedValidID = (uint*)0x00871E5C;
        internal static uint* ACCWeenieObject__splitStackSize = (uint*)0x00871EC4;
        internal static double* ACCWeenieObject__splitTime = (double*)0x00871EC8;
        internal static uint* ACCWeenieObject__prevRequest = (uint*)0x00871ED0;
        internal static uint* ACCWeenieObject__prevRequestObjectID = (uint*)0x00871ED4;
        internal static double* ACCWeenieObject__prevRequestTime = (double*)0x00871ED8;
        internal static int* ACCWeenieObject__attackInProgress = (int*)0x00871EE0;
        internal static int* ACCWeenieObject__splitClassID = (int*)0x00871F88;
        internal static double* PhysicsTimer__curr_time = (double*)0x008EEE70;
        internal static int CommandInterpreter { get { try { return (*(int*)(*Smartbox + 0xB8)); } catch { return 0; } } }
        internal static int CPhysics { get { try { return *CPhysicsPart__player_object; } catch { return 0; } } }
        internal static int MovementManager { get { try { return *(int*)(CPhysics + 0x00C4); } catch { return 0; } } }
        internal static int CMotionInterp { get { try { return *(int*)(MovementManager + 0x0000); } catch { return 0; } } }
        internal static int ClientNet { get { try { return (*(int*)(*gmClient + 0x0100)); } catch { return 0; } } }
        internal static int ReceiverData { get { try { return *(int*)(P.ClientNet + 0x8820); } catch { return 0; } } }
        internal static int UIPersistantData { get { try { return *(int*)(*(int*)(*P.gmClient + 0x00B0) + 0x0098); } catch { return 0; } } }

        internal static ushort* Device__m_DisplayPrefs_Resolution_Height = (ushort*)0x00818B64;
        internal static ushort* Device__m_DisplayPrefs_Resolution_Width = (ushort*)0x00818B66;
        internal static byte* Device__m_DisplayPrefs_LandscapeDetail = (byte*)0x0081FFA4;
        internal static byte* Device__m_DisplayPrefs_EnvironmentDetail = (byte*)0x0081FFA5;
        internal static uint* Device__m_DisplayPrefs_Landscape = (uint*)0x0081FFA8;
        internal static uint* Device__m_DisplayPrefs_Environment = (uint*)0x0081FFAC;
        internal static uint* Device__m_DisplayPrefs_SceneryDraw = (uint*)0x0081FFB0;
        internal static uint* Device__m_DisplayPrefs_LandscapeDraw = (uint*)0x0081FFB4;

        internal static int gmExternalContainerUI {
            get {
                if (*P.GlobalEventHandler == 0 || *(int*)(*P.GlobalEventHandler + 4) == 0) return 0;
                int m_handlers = *(int*)(*(int*)(*P.GlobalEventHandler + 4) + 0x1C);
                while (m_handlers != 0 && *(int*)m_handlers != 0x4DD1F9) m_handlers = *(int*)(m_handlers + 4);
                return *(int*)*(int*)(*(int*)(m_handlers + 8) + 4) - 1528;
            }
        }
        internal static int gmExternalContainerUI_m_itemList { get { try { return *(int*)(gmExternalContainerUI + 0x060C); } catch { return 0; } } }

        
        [DllImport("user32.dll")] internal static extern int GetActiveWindow();
        [DllImport("user32.dll")] internal static extern int GetForegroundWindow();
        #region Write(IntPtr address, <int,float,byte> newValue)
        internal static void Write(IntPtr address, int newValue) {
            try {
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, 0x40, out int b);
                *(int*)address = newValue;
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, b, out b);
            } catch { }
        }
        internal static void Write(IntPtr address, uint newValue) {
            try {
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, 0x40, out int b);
                *(uint*)address = newValue;
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.intsize, b, out b);
            } catch { }
        }
        internal static void Write(IntPtr address, float newValue) {
            try {
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.floatsize, 0x40, out int b);
                *(float*)address = newValue;
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.floatsize, b, out b);
            } catch { }
        }
        internal static void Write(IntPtr address, byte newValue) {
            try {
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.bytesize, 0x40, out int b);
                *(byte*)address = newValue;
                P.VirtualProtectEx(Process.GetCurrentProcess().Handle, address, P.bytesize, b, out b);
            } catch { }
        }
        #endregion
        #region PatchCall(int callLocation, IntPtr newPointer)
        internal static bool PatchCall(int callLocation, IntPtr newPointer) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return false;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            int newOffset = (int)newPointer - (callLocation + 5);
            Write((IntPtr)(callLocation + 1), newOffset);
            return true;
        }
        #endregion
        #region ReadCall(int callLocation)
        internal static int ReadCall(int callLocation) {
            if (((*(byte*)callLocation) & 0xFE) != 0xE8)
                return 0;
            int previousOffset = *(int*)(callLocation + 1);
            int previousPointer = previousOffset + (callLocation + 5);
            return previousPointer;
        }
        #endregion
        #region SwapBytes(<uint,ushort,short>)
        internal static uint SwapBytes(uint x) {
            x = (x >> 16) | (x << 16);
            return ((x & 0xFF00FF00) >> 8) | ((x & 0x00FF00FF) << 8);
        }
        internal static ushort SwapBytes(ushort x) {
            return (ushort)((x >> 8) | (x << 8));
        }
        internal static short SwapBytes(short x) {
            return (short)((x >> 8) | (x << 8));
        }
        #endregion
        [DllImport("kernel32.dll")] internal static extern bool VirtualProtectEx(IntPtr hProcess, IntPtr lpAddress, UIntPtr dwSize, int flNewProtect, out int lpflOldProtect);
    }
    public class Hooker {
        internal IntPtr Entrypoint;
        internal Delegate Del;
        internal int call;
        internal bool Hooked = false;
        internal unsafe bool Patch(IntPtr newPointer) {
            if (((*(byte*)call) & 0xFE) != 0xE8)
                return false;
            P.VirtualProtectEx(Process.GetCurrentProcess().Handle, (IntPtr)(call + 1), P.intsize, 0x40, out int b);
            *(int*)(call + 1) = (int)newPointer - (call + 5);
            P.VirtualProtectEx(Process.GetCurrentProcess().Handle, (IntPtr)(call + 1), P.intsize, b, out b);
            return true;
        }
        internal unsafe int Read() => call == 0 || ((*(byte*)call) & 0xFE) != 0xE8 ? 0 : *(int*)(call + 1) + (call + 5);
        public Hooker(int entrypoint, int call_location) {
            Entrypoint = (IntPtr)entrypoint;
            call = call_location;
        }
        public void Setup(Delegate del) {
            if (!Hooked) {
                Hooked = true;
                if (Read() != (int)Entrypoint) {
                    Core.WriteToDebugLog($"Failed to detour 0x{call:X8}. expected 0x{(int)Entrypoint:X8}, received 0x{Read():X8}");
                    return;
                }
                Del = del;
                if (!Patch(Marshal.GetFunctionPointerForDelegate(Del))) {
                    Del = null;
                    Hooked = false;
                }
            }
        }
        public void Remove() {
            if (Hooked) {
                Hooked = false;
                if (Patch(Entrypoint)) Del = null;
            }
        }
    }

public static unsafe class Jumper {

        public enum PowerBarMode {
            PBM_UNDEF = 0x0,
            PBM_COMBAT = 0x1,
            PBM_ADVANCED_COMBAT = 0x2,
            PBM_JUMP = 0x3,
            PBM_DDD = 0x4,
        };
    }
}
